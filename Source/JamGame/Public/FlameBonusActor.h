// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FlameBaseFunctionLibrary.h"
#include "PlayerFlameCharacter.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "FlameBonusActor.generated.h"

UCLASS()
class JAMGAME_API AFlameBonusActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFlameBonusActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* CollisionComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* BonusMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Info)
	FBonus CurrentBonusInfo;

	FTimerHandle TimerHandle_ReturnVal;

	
	IFlameBonusObjInterface* CurActor = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION(BlueprintCallable)
	virtual void ApplyBonus(APlayerFlameCharacter* CurPlayer=nullptr);

	void DisableActor(bool toHide);

	void ReturnVal();

	UFUNCTION(BlueprintNativeEvent)
	void PlayApplySoundBP();
	
};


