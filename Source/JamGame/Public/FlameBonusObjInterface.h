// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FlameBonusObjInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFlameBonusObjInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class JAMGAME_API IFlameBonusObjInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void Bonus_AddCoins(float IncVal);
	virtual void Bonus_AddHealth(float IncVal);
	virtual void Bonus_AddRespawn(float IncVal);

	virtual void Bonus_IncSpeedWalk(float IncVal, float Time);
	virtual void Bonus_IncDamage(float IncVal, float Time);
	virtual void Bonus_IncProjectileSpeed(float IncVal, float Time);

	virtual void Bonus_ApplyThumb(float IncVal);
	virtual void Bonus_ApplyMiddle(float IncVal);
	virtual void Bonus_ApplyRing(float IncVal);
	virtual void Bonus_ApplyLittle(float IncVal);
};
