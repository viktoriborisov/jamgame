// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DamagingActor.h"
#include "FlameBonusObjInterface.h"
#include "FlameWeaponActor.h"
#include "GameFramework/Character.h"
#include "PlayerFlameCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCoinsChange, float, CountCoins);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeClickFingers , bool, NewValue);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActivateDamageBonus, float, Time);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActivateSpeedBonus, float, Time);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActivateProjSpeedBonus, float, Time);

/**
 * 
 */
UCLASS()
class JAMGAME_API APlayerFlameCharacter : public ACharacter, public IFlameBonusObjInterface, public IDamagingActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;


public:

	APlayerFlameCharacter();
	
	/** Returns CameraBoom subobject **/
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
	class UFlameHealthComponent* HealthComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	AFlameWeaponActor* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	AFlameWeaponActor* CurrentMeleeWeapon = nullptr;
	
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Coins")
	FOnCoinsChange OnCoinsChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category=Cooldawn)
	FOnActivateDamageBonus OnActivateDamageBonus;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category=Cooldawn)
	FOnActivateSpeedBonus OnActivateSpeedBonus;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category=Cooldawn)
	FOnActivateProjSpeedBonus OnActivateProjSpeedBonus;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	bool bIsAiming{false};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	bool bIsAttack{false};
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* JumpAction;

	bool bIsOpenClickFingers = false;

	// Cool-dawns

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = Activate)
	FOnChangeClickFingers OnChangeClickFingers;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Activate)
	FTimerHandle TimerHandle_ActivateClickFingers;

	bool IsActiveClickFingers{true};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Activate)
	float TimeToActiveClickFingers = 10;

	void LockClickFingers();
	void ActivateClickFingers();

protected:
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PossessedBy(AController* NewController) override;

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
	void PlayTakeDamageSound_BP();

	UFUNCTION(BlueprintNativeEvent)
	void PlayDeadSound_BP();

	UFUNCTION(BlueprintNativeEvent)
	void PlayAimAnimation_BP();

	UFUNCTION(BlueprintNativeEvent)
	void PlayShootAnimation_BP(EWeaponType CurWeaponType);
	
	UFUNCTION()
	void CharDead();
	UFUNCTION()
	void CharHealthChange(float Health, float Damage);
	
	void MoveForward(float Value);
	void MoveRight(float Value);
	
	void ClickFingers();

	UFUNCTION(BlueprintCallable)
	void UseWeapon(FName NameWeapon, EAttackType CurAttackType,  bool IsOnce);

	void FireAction();
	void EndAimingAction();
	void StartAimingAction();

	UFUNCTION(BlueprintNativeEvent)
	void EndAimingAction_BP();

	UFUNCTION(BlueprintNativeEvent)
	void StartAimingAction_BP();
	
	void CenterViewportCursor(const APlayerController* PlayerController);

	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;


	////////////////////////////////
	/////// Bonus overriding methods 
	////////////////////////////////
	
	virtual void Bonus_AddCoins(float IncVal) override;
	virtual void Bonus_AddHealth(float IncVal) override;
	virtual void Bonus_AddRespawn(float IncVal) override;

	virtual void Bonus_IncSpeedWalk(float IncVal, float Time) override;
	virtual void Bonus_IncDamage(float IncVal, float Time) override;
	virtual void Bonus_IncProjectileSpeed(float IncVal, float Time) override;

	virtual void Bonus_ApplyThumb(float IncVal) override;
	virtual void Bonus_ApplyMiddle(float IncVal) override;
	virtual void Bonus_ApplyRing(float IncVal) override;
	virtual void Bonus_ApplyLittle(float IncVal) override;
};
