// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FlameBaseFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "FlameGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class JAMGAME_API UFlameGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DataTable)
	UDataTable* CurWeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DataTable)
	UDataTable* CurBonusInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = DataTable)
	UDataTable* CurLevelInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = DataTable)
	UDataTable* CurEnemiesInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Name)
	bool bIsFirstLaunch;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Name)
	FString CurPlayerName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sounds)
	float MainSoundVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sounds)
	float EffectSoundVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Coins)
	int Coins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Skills)
	TArray<FName> Skills{};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Records)
	TMap<int, FString> Records{};

	

	FDBStruct CurDBJson{};
	
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetBonusInfoByName(FName NameBonus, FBonus& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetEnemiesInfoByName(FName NameEnemy, FEnemiesInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
	void GetRandomBonus(FBonus& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool LoadDB();

	UFUNCTION(BlueprintCallable)
	void SaveDB();

	FString GetPathToDB();

	UFUNCTION(BlueprintCallable)
	TArray<FString> GetCSVFile(FString Path);

	UFUNCTION(BlueprintCallable)
	void  LoadCSVDataFile(ECSVTableType CurTableType, EDifficultyLevel CurDiffLevel);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DB)
	EDifficultyLevel DiffLevel = EDifficultyLevel::Simple;
};
