// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerFlameCharacter.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PlayerStart.h"
#include "FlamePlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCountTatemChange, float, CountTatems);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNextEnemyWave, float, CountEnemy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeActiveEnemy, float, NewVal);

/**
 * 
 */
UCLASS()
class JAMGAME_API AFlamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AFlamePlayerController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Respawn)
	APlayerFlameCharacter* CurrentPlayerCharacter = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = SurfacePlatform)
	TSubclassOf<AActor> ClassSurfacePlatform = nullptr;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Tatem")
	FOnCountTatemChange OnCountTatemChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category=Enemies)
	FOnChangeActiveEnemy OnChangeActiveEnemy;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category=Enemies)
	FOnNextEnemyWave OnNextEnemyWave;
	
	UPROPERTY(EditDefaultsOnly, Category = SurfacePlatform)
	AActor* SurfacePlatform = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = SurfacePlatform)
	float SurfaceXfactor = 200;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = SurfacePlatform)
	float SurfaceYfactor = 200;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Tatem)
	int CountFreeTatem = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Tatem)
	int CountFreeTatemForPray = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Tatem)
	int CountOfPrey = 1;
	

	UPROPERTY(EditDefaultsOnly, Category = EnemyType)
	TSubclassOf<class AEnemyFlameCharacter> ClassEnemiesMeleeAttack = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = EnemyType)
	TSubclassOf<class AEnemyFlameCharacter> ClassEnemiesDistanceAttack = nullptr;

	/*UPROPERTY()
	UFlameGameInstance* CurGameInstance;*/
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	int BeginRespawnCountEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	int MaxRespawnCountEnemies;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	int IncRespawnCountEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	float TimeToRespawnEnemies;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Respawn)
	float ChanceSpawnMeleeEnemy;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Respawn)
	float ChanceSpawnDistanceEnemy;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Respawn)
	FTimerHandle TimerHandle_Respawn;

	UFUNCTION(BlueprintCallable)
	void ChangeTatemCount();

	FIntPoint m_lastMousePosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	TMap<FString, AActor*> MapEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	TArray<APlayerStart*> EnemyStartPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn)
	APlayerStart* PlayerStartPoint;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Enemies)
	int CountActiveEnemies;

	void SetMouse();

	void NotifyEnemyDeath();

protected:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* aPawn) override;

public:
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = Respawn)
	void SetSpawnPlatform();

	UFUNCTION(BlueprintCallable, Category = Respawn)
	void SpawnEnemyActor();

private:
	bool bIsStartGame = true;
};
