// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DamagingActor.h"
#include "FlameBonusObjInterface.h"
#include "PlayerFlameCharacter.h"
#include "Components/SphereComponent.h"
#include "EnemyFlameCharacter.generated.h"

/**
 * 
 */
UCLASS()
class JAMGAME_API AEnemyFlameCharacter : public ACharacter, public IDamagingActor
{
	GENERATED_BODY()

public:

	AEnemyFlameCharacter();

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* CollisionComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	USkeletalMeshComponent* PawnSkeletalMesh = nullptr;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
	UFlameHealthComponent* HealthComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyType)
	EEnemyType CutEnemyType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	AFlameWeaponActor* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	UAnimMontage* AnimAttack = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	UAnimMontage* AnimDeath = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	UAnimMontage* AnimTakeDamage = nullptr;

	UPROPERTY()
	AFlamePlayerController* PlController = nullptr;
	
	UPROPERTY()
	APlayerFlameCharacter* PlChar = nullptr;

	FTimerHandle TimerHandle_PlayingDeath;
	

	float CurrentDeltaTime;
	float PastShootingTime;



protected:
	UFUNCTION()
	void PawnDead();
	
	UFUNCTION()
	void PawnHealthChange(float Health, float Damage);
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PossessedBy(AController* NewController) override;

	
	

public:
	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator,
		AActor* DamageCauser) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Attack();

	void SetParams(int HealthInPercent, int MaxSpeed);

	void SpawnBonus();

	UFUNCTION(BlueprintNativeEvent)
	void PlayDamageAnim_BP();

	UFUNCTION(BlueprintNativeEvent)
	void PlayDeathAnim_BP();

	UFUNCTION(BlueprintNativeEvent)
	void PlayAttackAnim_BP();

	UFUNCTION(BlueprintCallable)
	void ChangeSpeed(float FactorSpeed);
	////////////////////////////////
	/////// Bonus overriding methods 
	////////////////////////////////
	
	/*virtual void Bonus_AddCoins(float IncVal) override;
	virtual void Bonus_AddHealth(float IncVal) override;
	virtual void Bonus_AddRespawn(float IncVal) override;

	virtual void Bonus_IncSpeedWalk(float IncVal) override;
	virtual void Bonus_IncDamage(float IncVal) override;
	virtual void Bonus_IncProjectileSpeed(float IncVal) override;*/

};
	

