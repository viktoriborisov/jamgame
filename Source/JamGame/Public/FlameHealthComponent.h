// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FlameHealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeCountLive, int, CountLive);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JAMGAME_API UFlameHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFlameHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Health")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Health")
	FOnDead OnDead;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Health")
	FOnChangeCountLive OnChangeCountLive;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health")
	bool bIsAlive = true;
	//bool bIsRespawn = false;

	float MaxHealth = 100.0f;
	float Health = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health")
	int CountLife = 1;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health")
	float CoefDamage = 1.0f;

	UFUNCTION(BlueprintCallable, Category="Health")
	float GetCurrentHealth();
	
	UFUNCTION(BlueprintCallable, Category="Health")
	void SetCurrentHealth(float NewHealth);


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category="Health")
	virtual void ChangeHealthValue(float ChangeValue);

};
