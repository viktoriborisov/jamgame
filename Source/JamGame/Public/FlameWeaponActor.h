// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FlameBaseFunctionLibrary.h"

#include "GameFramework/Actor.h"
#include "FlameWeaponActor.generated.h"

UCLASS()
class JAMGAME_API AFlameWeaponActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFlameWeaponActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	USceneComponent* SceneComponent = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMeshWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
	AActor* HandleOwner = nullptr;

	UPROPERTY()
	APlayerController* CurPlayerController = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Info)
	FWeaponInfo CurrentWeaponInfo;

	FVector WorldLocation = FVector::ZeroVector;
	FVector WorldDirection = FVector::ZeroVector;
	FVector EndLocProjectile = FVector::ZeroVector;

	EOwnerType OwnerType;
	
	FName NameWeapon;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Fire();

	UFUNCTION(BlueprintNativeEvent)
	void PlayFireSound_BP(USoundBase* SoundFireWeapon);

	void SetCurPlayerController(APlayerController* NewController);

	void CalculateEndLocationOfProjectile();
	void SpawnProjectile(const FVector SpawnLocation, const FVector TargetVector);

};
