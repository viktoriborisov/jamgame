// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyFlameCharacter.h"
#include "FlameAIController.generated.h"

/**
 * 
 */
UCLASS()
class JAMGAME_API AFlameAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=ControlledPawn)
	AEnemyFlameCharacter* CurPawn = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Attack)
	bool bIsCanAttack;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Attack)
	float AttackDistance = 100.0f;


	FVector PastAttackDirPlayer;

	FVector PastTargetCircleDir;
	FVector PastTargetCommonDir;
	bool bIsEqualTargetCommonDir;
	bool bIsSurrounds;

	FVector TargetRoundLeft;
	FVector TargetRoundRight;
	
	FVector TargetRoundFullLeft;
	FVector TargetRoundFullRight;

	FVector PastVectorPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=MeleeEnemy)
	ESides CurSide;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=MeleeEnemy)
	float RadiusLength = 200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DistanceEnemy)
	bool bIsSafeZone;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DistanceEnemy)
	float SafeDistance = 300.0f;
	
private:
	float TickToDealLogicTime = 0.01;
	float CurDeltaTime;
	float PastCurDeltaTime;
	float MoveDeltaTime;
	float MoveDeltaTimeSec;

	FVector ConstDeltaZ;

	bool bIsGetRoundLeft = false;
	bool bIsGetRoundRight = false;

	bool bIsGetRoundFullLeft = false;
	bool bIsGetRoundFullRight = false;


	FName CurNameOfEnemy = "MeleeEnemy";

protected:
	void StartMeleeLogic(float DeltaTime);
	void StartDistanceLogic(float DeltaTime);

public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnPossess(APawn* InPawn) override;
	UFUNCTION(BlueprintCallable)
	void SetRadiuses();
	UFUNCTION(BlueprintNativeEvent)
	void TypeWasDefined_BP();


};

