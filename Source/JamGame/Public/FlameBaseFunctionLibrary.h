// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "FlameBaseFunctionLibrary.generated.h"

/**
 * 
 */


UENUM(BlueprintType)
enum class ECSVTableType : uint8
{
	Levels UMETA(DisplayName = Levels),
	Enemies UMETA(DisplayName = Enemies),
	Bonuses UMETA(DisplayName = Bonuses),
	Weapons UMETA(DisplayName = Weapons)
};


USTRUCT(BlueprintType)
struct FEnemiesInfo : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
	int HealthInPercent = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Speed)
	int MaxSpeed = 490;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sight)
	int SightRadius = 600;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sight)
	int LoseSightRadius = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sight)
	float PeripheralVisionAngleDegrees = 56;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hearing)
	int HearingRange = 400;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hearing)
	int TeamRange = 1000;
};



USTRUCT(BlueprintType)
struct FDBStruct : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Name)
	bool bIsFirstLaunch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Name)
	FString CurPlayerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DiffLevel)
	int DiffLevel;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sounds)
	float MainSoundVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Sounds)
	float EffectSoundVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Coins)
	int Coins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Skills)
	TArray<FName> Skills{};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Records)
	TMap<int, FString> Records{};
};


UENUM(BlueprintType)
enum class ESides : uint8
{
	Front UMETA(DisplayName = Front),
	Back UMETA(DisplayName = Back),
	Left UMETA(DisplayName = Left),
	Right UMETA(DisplayName = Right)
};

UENUM(BlueprintType)
enum class EEnemyType : uint8
{
	MeleeEnemy UMETA(DisplayName = MeleeEnemy),
	DistanceEnemy UMETA(DisplayName = DistanceEnemy),
};


UENUM(BlueprintType)
enum class EDifficultyLevel : uint8
{
	Simple UMETA(DisplayName = Simple),
	Middle UMETA(DisplayName = Middle),
	Hard UMETA(DisplayName = Hard)
};


USTRUCT(BlueprintType)
struct FLevelInfo : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RespawnEnemies)
	int BeginRespawnCountEnemies = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RespawnEnemies)
	int MaxRespawnCountEnemies = 25;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RespawnEnemies)
	int IncRespawnCountEnemies = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RespawnEnemies)
	float TimeToRespawnEnemies = 20.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = RespawnChance)
	float ChanceSpawnMeleeEnemy = 60;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = RespawnChance)
	float ChanceSpawnDistanceEnemy = 40;
};


UENUM(BlueprintType)
enum class EBonusName : uint8
{
	LittleRing			UMETA(DisplayName = LittleRing),
	RingRing			UMETA(DisplayName = RingRing),
	MiddleRing			UMETA(DisplayName = MiddleRing),
	IndexRing			UMETA(DisplayName = IndexRing),
	ThumbRing			UMETA(DisplayName = ThumbRing),
	
	Coins				UMETA(DisplayName = Coins),
	Health				UMETA(DisplayName = Health),
	Respawn				UMETA(DisplayName = Respawn),

	IncSpeedWalk		UMETA(DisplayName = IncSpeedWalk),
	IncDamage			UMETA(DisplayName = IncDamage),
	IncProjectileSpeed	UMETA(DisplayName = IncProjectileSpeed)
};

UENUM(BlueprintType)
enum class EBonusType : uint8
{
	Common UMETA(DisplayName = Common),
	Skill  UMETA(DisplayName = Skill),
	Temp   UMETA(DisplayName = Temp)
};


UENUM(BlueprintType)
enum class EBonusSpawn : uint8
{
	Player UMETA(DisplayName = Player),
	Enemy  UMETA(DisplayName = Enemy),
	Surface   UMETA(DisplayName = Surface)
};


USTRUCT(BlueprintType)
struct FBonus : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Class)
	TSubclassOf<class AFlameBonusActor> BonusClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Common)
	EBonusName CurBonusName = EBonusName::Coins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Common)
	float IncValue = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Common)
	EBonusType CurBonusType = EBonusType::Common;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Common)
	EBonusSpawn CurBonusSpawn = EBonusSpawn::Player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Common)
	float ChanceToSpawn = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attach)
	FName AttachSocketName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TempParam)
	float LifeTime = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cost)
	int Cost = 0.0f;
};


UENUM(BlueprintType)
enum class EOwnerType : uint8
{
	Player UMETA(DisplayName = Player),
	Enemy UMETA(DisplayName = Enemy)
};

UENUM(BlueprintType)
enum class EProjectileType : uint8
{
	Simple UMETA(DisplayName = Simple),
	Grenade UMETA(DisplayName = Grenade)
};

USTRUCT(BlueprintType)
struct FProjectileSetting
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	TSubclassOf<class AFlameProjectileActor> Projectile = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	UStaticMeshComponent* ProjectileStaticMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= ProjectileSetting)
	EProjectileType ProjectileType = EProjectileType::Simple;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	float MaxDamageDistance = 100.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	float MinDamageDistance = 200.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	float InitialSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	float MaxSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	float ProjectileLifeTime = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	UParticleSystem* ProjectileTrailFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	UNiagaraSystem* ExploseFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileSetting)
	USoundBase* ExploseSound = nullptr;

	EOwnerType OwnerType;
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	PlayerPistol			UMETA(DisplayName = PlayerPistol),
	PlayerGrenade			UMETA(DisplayName = PlayerGrenade),
	SlitWeapon				UMETA(DisplayName = SlitWeapon),
	ClickFingers			UMETA(DisplayName = ClickFingers),
	EnemyMeleeWeapon		UMETA(DisplayName = EnemyMeleeWeapon),
	EnemyDistanceWeapon		UMETA(DisplayName = EnemyDistanceWeapon),
};

UENUM(BlueprintType)
enum class EShootingType : uint8
{
	Auto UMETA(DisplayName = Auto),
	OnceAuto UMETA(DisplayName = OnceAuto),
	Once UMETA(DisplayName = Once)
};

USTRUCT(BlueprintType)
struct FFiringAnimationInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharAnim)
	UAnimMontage* AnimCharFire = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAnim)
	UAnimMontage* AnimWeaponFire = nullptr;
	
};

UENUM(BlueprintType)
enum class EAttackType : uint8
{
	Melee UMETA(DisplayName = Melee),
	Distance UMETA(DisplayName = Distance)
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Class)
	TSubclassOf<class AFlameWeaponActor> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	EWeaponType WeaponType = EWeaponType::PlayerPistol;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	EShootingType ShootingType = EShootingType::Auto;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	float RateOfFire = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	int32 NumberProjectileByShot = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	float Damage = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	float MaxDamageDistance = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Specifications)
	float MinDamageDistance = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Location)
	FName NameAttachSocket = "SocketRightHand";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
	FProjectileSetting ProjectileSetting;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sound)
	USoundBase* SoundFireWeapon = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FX)
	UNiagaraSystem* EffectFireWeapon = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HitDecal)
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
	FFiringAnimationInfo AnimationOfFiring;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Inventory)
	UTexture2D* WeaponIcon = nullptr;
	
};


UCLASS()
class JAMGAME_API UFlameBaseFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static AFlameWeaponActor* SetWeapon(UWorld* CurUWorld,
									    USkeletalMeshComponent* TargetMesh,
									    AActor* Owner,
									    APawn* Instigator,
									    AFlamePlayerController* CurPlayerController,
									    FName NameWeapon);
	
};
