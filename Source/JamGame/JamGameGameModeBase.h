// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JamGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JAMGAME_API AJamGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
