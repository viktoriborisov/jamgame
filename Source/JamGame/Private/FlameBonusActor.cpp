// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameBonusActor.h"

#include "FlameGameInstance.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFlameBonusActor::AFlameBonusActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!CollisionComponent)
	{
		// Use a sphere as a simple collision representation.
		CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
		// Set the sphere's collision profile name to "Projectile".
		CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Bonus"));
		// Event called when component hits something.
		CollisionComponent->OnComponentHit.AddDynamic(this, &AFlameBonusActor::OnHit);
		CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AFlameBonusActor::OverlapBegin);
		// Set the sphere's collision radius.
		CollisionComponent->InitSphereRadius(15.0f);
		// Set the root component to be the collision component.
		RootComponent = CollisionComponent;
	}

	if (!BonusMesh)
	{
		BonusMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
		BonusMesh->SetupAttachment(RootComponent);
		BonusMesh->SetCanEverAffectNavigation(false);
	}

}

// Called when the game starts or when spawned
void AFlameBonusActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFlameBonusActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFlameBonusActor::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit)
{
	/*IFlameBonusObjInterface* NewActor = Cast<IFlameBonusObjInterface>(OtherActor);
	if (NewActor && CurrentBonusInfo.CurBonusType != EBonusType::Skill)
	{
		CurActor = NewActor;
		ApplyBonus();
	}*/
}

void AFlameBonusActor::OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	IFlameBonusObjInterface* NewActor = Cast<IFlameBonusObjInterface>(OtherActor);
	if (NewActor && CurrentBonusInfo.CurBonusType != EBonusType::Skill)
	{
		CurActor = NewActor;
		ApplyBonus();
	}
}

void AFlameBonusActor::ApplyBonus(APlayerFlameCharacter* CurPlayer) 
{
	if (APlayerFlameCharacter* NewCastActor = Cast<APlayerFlameCharacter>(CurActor))
		PlayApplySoundBP();
	
	if (CurrentBonusInfo.CurBonusType == EBonusType::Common)
	{
		DisableActor(true);
		switch (CurrentBonusInfo.CurBonusName) {
			case EBonusName::Coins:
				CurActor->Bonus_AddCoins(CurrentBonusInfo.IncValue);
				break;
			case EBonusName::Health:
				CurActor->Bonus_AddHealth(CurrentBonusInfo.IncValue);
				break;
			case EBonusName::Respawn:
				CurActor->Bonus_AddRespawn(CurrentBonusInfo.IncValue);
				break;
			default: ;
		}
		Destroy();
	}
	else if (CurrentBonusInfo.CurBonusType == EBonusType::Temp)
	{
		DisableActor(true);
		switch (CurrentBonusInfo.CurBonusName) {
			case EBonusName::IncSpeedWalk:
				CurActor->Bonus_IncSpeedWalk(CurrentBonusInfo.IncValue, CurrentBonusInfo.LifeTime);
				break;
			case EBonusName::IncDamage:
				CurActor->Bonus_IncDamage(CurrentBonusInfo.IncValue, CurrentBonusInfo.LifeTime);
				break;
			case EBonusName::IncProjectileSpeed:
				CurActor->Bonus_IncProjectileSpeed(CurrentBonusInfo.IncValue, CurrentBonusInfo.LifeTime);
				break;
			default: ;
		}
		

		GetWorldTimerManager().SetTimer(TimerHandle_ReturnVal,
										this,
		                                &AFlameBonusActor::ReturnVal,
		                                CurrentBonusInfo.LifeTime,
		                                true);
	}
	else if (CurrentBonusInfo.CurBonusType == EBonusType::Skill)
	{
		if (CurPlayer)
		{
			switch (CurrentBonusInfo.CurBonusName) {
			case EBonusName::LittleRing:
				CurPlayer->Bonus_ApplyLittle(CurrentBonusInfo.IncValue);
				break;
			case EBonusName::RingRing:
				CurPlayer->Bonus_ApplyRing(CurrentBonusInfo.IncValue);
				break;
			case EBonusName::MiddleRing:
				CurPlayer->Bonus_ApplyMiddle(CurrentBonusInfo.IncValue);
				break;
			case EBonusName::ThumbRing:
				CurPlayer->Bonus_ApplyThumb(CurrentBonusInfo.IncValue);
				break;
			default: ;
			}
		}
	}
}

void AFlameBonusActor::ReturnVal()
{
	switch (CurrentBonusInfo.CurBonusName) {
	case EBonusName::IncSpeedWalk:
		CurActor->Bonus_IncSpeedWalk(1/CurrentBonusInfo.IncValue, 0);
		break;
	case EBonusName::IncDamage:
		CurActor->Bonus_IncDamage(1/CurrentBonusInfo.IncValue, 0);
		break;
	case EBonusName::IncProjectileSpeed:
		CurActor->Bonus_IncProjectileSpeed(1/CurrentBonusInfo.IncValue, 0);
		break;
	default: ;
	}
	GetWorldTimerManager().ClearTimer(TimerHandle_ReturnVal);
}

void AFlameBonusActor::PlayApplySoundBP_Implementation()
{
}



void AFlameBonusActor::DisableActor(bool toHide) 
{
	// Hides visible components
	SetActorHiddenInGame(toHide);

	// Disables collision components
	SetActorEnableCollision(false);

	// Stops the Actor from ticking
	SetActorTickEnabled(false);
}


