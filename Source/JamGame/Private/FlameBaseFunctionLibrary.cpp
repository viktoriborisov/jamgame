// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameBaseFunctionLibrary.h"

#include "FlameGameInstance.h"
#include "FlamePlayerController.h"
#include "FlameWeaponActor.h"
#include "PlayerFlameCharacter.h"

AFlameWeaponActor* UFlameBaseFunctionLibrary::SetWeapon(UWorld* CurUWorld,
                                                        USkeletalMeshComponent* TargetMesh,
                                                        AActor* CurOwner,
                                                        APawn* CurInstigator,
                                                        AFlamePlayerController* CurPlayerController,
                                                        FName NameWeapon)
{
	UFlameGameInstance* MyGI = Cast<UFlameGameInstance>(CurUWorld->GetGameInstance());
	if (MyGI)
	{
		FWeaponInfo MyInfo;
		if (MyGI->GetWeaponInfoByName(NameWeapon, MyInfo))
		{
			const FVector CurSpawnLocation = FVector::ZeroVector;
			const FRotator CurSpawnRotation = FRotator::ZeroRotator;

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = CurOwner;
			SpawnParams.Instigator = CurInstigator;
			
			AFlameWeaponActor* NewWeapon = Cast<AFlameWeaponActor>(CurUWorld->SpawnActor(MyInfo.WeaponClass,
																		&CurSpawnLocation,
																		&CurSpawnRotation,
																		SpawnParams));
			if (NewWeapon)
			{
				const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
				NewWeapon->AttachToComponent(TargetMesh, Rule, MyInfo.NameAttachSocket);
				
				NewWeapon->NameWeapon = NameWeapon;
				NewWeapon->CurrentWeaponInfo = MyInfo;

				NewWeapon->SetCurPlayerController(CurPlayerController);

				if (Cast<APlayerFlameCharacter>(CurOwner))
				{
					NewWeapon->OwnerType = EOwnerType::Player;
				}
				else
				{
					NewWeapon->OwnerType = EOwnerType::Enemy;
				}

				NewWeapon->HandleOwner = CurOwner;

				return NewWeapon;
			}
		}
	}
	return nullptr;
}
