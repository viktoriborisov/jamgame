// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameWeaponActor.h"

#include "EnemyFlameCharacter.h"
#include "FlameHealthComponent.h"
#include "FlamePlayerController.h"
#include "FlameProjectileActor.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFlameWeaponActor::AFlameWeaponActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh "));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AFlameWeaponActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFlameWeaponActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/** implementation method from Interface IIArenaPawn */
void AFlameWeaponActor::Fire()
{

	/*if (CurCharacter)
	{
		//CurCharacter->PlayAnimMontage(CurrentWeaponInfo.AnimationOfFiring.AnimCharFire);
	}*/

	if (CurPlayerController && CurPlayerController->GetCharacter())
	{

		PlayFireSound_BP(CurrentWeaponInfo.SoundFireWeapon);
		
		if (OwnerType == EOwnerType::Player)
		{
			CurPlayerController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);

			CalculateEndLocationOfProjectile();
		}
		else
		{
			EndLocProjectile = CurPlayerController->GetPawn()->GetActorLocation();
		}

		switch (CurrentWeaponInfo.WeaponType)
		{
			case EWeaponType::SlitWeapon:
				{
					FCollisionQueryParams QueryParams;
					QueryParams.AddIgnoredActor(this);
					
					FHitResult HitForward;
			
					FVector TraceStartForward = HandleOwner->GetActorLocation() + HandleOwner->GetActorForwardVector()*80;
					FVector TraceEndForward = HandleOwner->GetActorLocation() + HandleOwner->GetActorForwardVector()*CurrentWeaponInfo.MinDamageDistance;

					GetWorld()->LineTraceSingleByChannel(HitForward, TraceStartForward, TraceEndForward, ECollisionChannel::ECC_WorldDynamic, QueryParams);
					//DrawDebugLine(GetWorld(), TraceStartForward, TraceEndForward, HitForward.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);

					if (AActor* DamagedActor = HitForward.GetActor())
					{
						if (DamagedActor->GetName().Contains("Enemy"))
						{
							UGameplayStatics::ApplyDamage(DamagedActor,
														  CurrentWeaponInfo.Damage,
														  GetInstigatorController(),
														  this,
														  UDamageType::StaticClass());
						}
					}
				}
				break;
			case EWeaponType::ClickFingers:
				{
					if (CurrentWeaponInfo.EffectFireWeapon)
					{
						UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), CurrentWeaponInfo.EffectFireWeapon, GetActorLocation(), FRotator::ZeroRotator, FVector(5.0f));
					}
					
					for (TObjectIterator<AEnemyFlameCharacter> Itr; Itr; ++Itr)
					{
						if (*Itr && Itr->GetDistanceTo(HandleOwner) < CurrentWeaponInfo.MinDamageDistance)
						{
							//UArenaCommonTypes::RepulseEnemy(*Itr, CurrentWeaponInfo.TargetVector_1);
							FVector DirToJump = Itr->GetActorLocation() - HandleOwner->GetActorLocation();
							DirToJump.Normalize();
							FVector JumpPoint = DirToJump*500 + FVector(0, 0, 500);
			
							Itr->LaunchCharacter(JumpPoint, true, true);

							Itr->HealthComponent->ChangeHealthValue(-CurrentWeaponInfo.Damage);
						}
					}

					if (APlayerFlameCharacter* MyPlayer = Cast<APlayerFlameCharacter>(HandleOwner))
					{
						MyPlayer->LockClickFingers();
					}
				}
				break;
			case EWeaponType::EnemyMeleeWeapon:
				{
					FVector DifferenceVec = CurPlayerController->GetCharacter()->GetActorLocation() + FVector(0, 0, 90) - GetActorLocation();
					const float VecLength = sqrt(pow(DifferenceVec.X, 2) + pow(DifferenceVec.Y, 2));
					
					UGameplayStatics::ApplyDamage(CurPlayerController->GetCharacter(),
												  CurrentWeaponInfo.Damage,
												  GetInstigatorController(),
												  this,
												  UDamageType::StaticClass());
					
				}
				break;
			default:
				SpawnProjectile(ShootLocation->GetComponentLocation() + GetActorForwardVector(), EndLocProjectile);
				break;
		}
	}
}


/** calculating projectile's "end location"  */
void AFlameWeaponActor::CalculateEndLocationOfProjectile()
{
	
	/*FHitResult HitTrace;
	FCollisionQueryParams Params;
	FCollisionResponseParams ResponseParam;
	Params.bReturnPhysicalMaterial = true;
				
	GetWorld()->LineTraceSingleByChannel(HitTrace,
										 WorldLocation,
										 WorldLocation + WorldDirection*1000,
										 ECC_PhysicsBody,
										 Params,
										 ResponseParam);

	/*if (HitTrace.GetActor() == this && FVector::Dist(EndLocProjectile, GetActorLocation()) < 200)
	{
		// UE_LOG(LogTemp, Warning, TEXT("Be careful you shoot yourself!"));
	}
	else#1# if (HitTrace.GetActor())
	{
		EndLocProjectile = HitTrace.ImpactPoint;
	}
	else
	{
		EndLocProjectile = WorldLocation + WorldDirection*1000;
	}*/

	//EndLocProjectile = EndLocProjectile * FVector(1, 1, 0) + FVector(0, 0, ShootLocation->GetComponentLocation().Z);

	/*if (FVector::Dist(EndLocProjectile, GetActorLocation()) < 200)
	{
		EndLocProjectile = ShootLocation->GetComponentLocation() + GetActorForwardVector() * 10000;
	}*/
	/*AFlamePlayerController* player = Cast<AFlamePlayerController>(Execute_GetMyOwner(this));
	if (player && player->SurfacePlatform) {
		EndLocProjectile.Z = player->SurfacePlatform->GetActorLocation().Z;
	}*/
	EndLocProjectile = ShootLocation->GetComponentLocation() + GetActorForwardVector()*20000;
}

/** start after action "FireAction" only on server Controller */
void AFlameWeaponActor::SpawnProjectile(const FVector SpawnLocation, const FVector TargetVector)
{
	if (CurPlayerController)
	{
		
		const FRotator SpawnRotation = CurPlayerController->GetControlRotation();

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();
	
		AFlameProjectileActor* MyProjectile = Cast<AFlameProjectileActor>(GetWorld()->SpawnActor(CurrentWeaponInfo.ProjectileSetting.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (MyProjectile)
		{
			CurrentWeaponInfo.ProjectileSetting.OwnerType = OwnerType;
			MyProjectile->InitProjectile(HandleOwner,
								         CurrentWeaponInfo.ProjectileSetting,
										 TargetVector,
										 CurrentWeaponInfo.Damage,
										 CurrentWeaponInfo.MaxDamageDistance,
										 CurrentWeaponInfo.MinDamageDistance);
			
			//for (int i = 0; i < EffectsToProjectiles.Num(); i++)
			/*MyProjectile->SetMyOwner(Execute_GetMyOwner(this));
			MyProjectile->Execute_ApplyAllEffects(MyProjectile, EffectsToProjectiles, 0);*/
		}
	}
}

void AFlameWeaponActor::SetCurPlayerController(APlayerController* NewController)
{
	CurPlayerController = NewController;
}

void AFlameWeaponActor::PlayFireSound_BP_Implementation(USoundBase* SoundFireWeapon)
{
	// in BP
}
