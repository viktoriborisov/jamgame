// Fill out your copyright notice in the Description page of Project Settings.


#include "FlamePlayerController.h"

#include "EnemyFlameCharacter.h"
#include "FlameGameInstance.h"
#include "FlameHealthComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"

AFlamePlayerController::AFlamePlayerController()
{
}

void AFlamePlayerController::SetMouse()
{
	FViewport* viewPort = CastChecked<ULocalPlayer>(this->Player)->ViewportClient->Viewport;
	if (viewPort->HasMouseCapture())
	{
		viewPort->SetMouse(m_lastMousePosition.X, m_lastMousePosition.Y);
	}
}

void AFlamePlayerController::NotifyEnemyDeath()
{
	CountActiveEnemies -= 1;
	OnChangeActiveEnemy.Broadcast(CountActiveEnemies);
}

void AFlamePlayerController::BeginPlay()
{
	Super::BeginPlay();

	UFlameGameInstance* CurGameInstance = Cast<UFlameGameInstance>(GetGameInstance());
	
}

void AFlamePlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
	
	if (APlayerFlameCharacter* MyCharacter = Cast<APlayerFlameCharacter>(aPawn))
	{
		CurrentPlayerCharacter = MyCharacter;
	}
}






void AFlamePlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	/*FViewport* viewPort = CastChecked<ULocalPlayer>(this->Player)->ViewportClient->Viewport;
	if (viewPort->HasMouseCapture())
	{
		viewPort->SetMouse(m_lastMousePosition.X, m_lastMousePosition.Y);
	}
	else
	{
		// remember last mouse pos
		FIntPoint lastMousePosition;
		viewPort->GetMousePos(lastMousePosition, true);

		if (lastMousePosition.X >= 0 && lastMousePosition.Y >= 0)
		{
			// skip if mouse is outside window (when outside window, X and Y become -1)

			float viewportRatio = 1.0f; // use 1.0 for standalone builds etc    			
			if (viewPort->IsPlayInEditorViewport())
			{
				FIntPoint screenResolutionViewport(GSystemResolution.ResX, GSystemResolution.ResY);
				viewportRatio = (float)screenResolutionViewport.Y / (float)screenResolutionViewport.X;
			}

			m_lastMousePosition.X = (float)lastMousePosition.X * viewportRatio;
			m_lastMousePosition.Y = (float)lastMousePosition.Y * viewportRatio;
		}
	}*/
}

void AFlamePlayerController::SetSpawnPlatform()
{
	if (PlayerStartPoint)
	{
		CurrentPlayerCharacter->SetActorLocation(PlayerStartPoint->GetActorLocation());
	}
		
	
	UFlameGameInstance* CurGameInstance = Cast<UFlameGameInstance>(GetGameInstance());
	if (CurGameInstance->CurLevelInfo)
	{
		int StartStr = GetWorld()->GetMapName().Find("Level");
		FString MapName = GetWorld()->GetMapName().Mid(StartStr, GetWorld()->GetMapName().Len() - StartStr);
		FLevelInfo* Result = CurGameInstance->CurLevelInfo->FindRow<FLevelInfo>(*MapName, *MapName, true);
		BeginRespawnCountEnemies = Result->BeginRespawnCountEnemies;
		MaxRespawnCountEnemies = Result->MaxRespawnCountEnemies;
		IncRespawnCountEnemies = Result->IncRespawnCountEnemies;
		TimeToRespawnEnemies = Result->TimeToRespawnEnemies;
		ChanceSpawnMeleeEnemy = Result->ChanceSpawnMeleeEnemy;
		ChanceSpawnDistanceEnemy = Result->ChanceSpawnDistanceEnemy;
	}

	
	TArray<AActor*> FoundPlatform;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassSurfacePlatform, FoundPlatform);
	if (FoundPlatform.Num() != 0)
	{
		SurfacePlatform = FoundPlatform[0];
	}
	SpawnEnemyActor();

	if (bIsStartGame)
	{
		bIsStartGame = false;
		/*GetWorldTimerManager().SetTimer(TimerHandle_Respawn,
									this,
									&AFlamePlayerController::SpawnEnemyActor,
									TimeToRespawnEnemies,
									true);*/
	}
}


void AFlamePlayerController::ChangeTatemCount()
{
	SpawnEnemyActor();
	CountFreeTatem += 1;
	OnCountTatemChange.Broadcast(CountFreeTatem);
}

void AFlamePlayerController::SpawnEnemyActor()
{

	const FVector SurfaceLoc = SurfacePlatform->GetActorLocation();
	const FVector Scale = SurfacePlatform->GetActorScale();

	const int Y_Min = 0;
	const int Y_Max = SurfaceLoc.Y + Scale.Y * SurfaceYfactor;

	const int X_Min = 0;
	const int X_Max = SurfaceLoc.X + Scale.X * SurfaceXfactor;

	const int Z_Num = SurfaceLoc.Z + 40;

	CountActiveEnemies += BeginRespawnCountEnemies;
	OnNextEnemyWave.Broadcast(CountActiveEnemies);
	
	for (int i = 0; i < BeginRespawnCountEnemies; ++i)
	{
		
		if (CurrentPlayerCharacter && SurfacePlatform)
		{
			
			const int X_Num = FMath::RandRange(X_Min, X_Max);
			const int Y_Num = FMath::RandRange(Y_Min, Y_Max);


			FVector SpawnLoc = FVector(X_Num, Y_Num, Z_Num);
			FRotator SpawnRot = FRotator::ZeroRotator;
			if (EnemyStartPoints[i % EnemyStartPoints.Num()])
			{
				SpawnLoc = EnemyStartPoints[i % EnemyStartPoints.Num()]->GetActorLocation();
                //SpawnRot = EnemyStartPoints[i % EnemyStartPoints.Num()]->GetArrowComponent()->GetComponentRotation();
			}

			int RandVal = FMath::RandRange(0, 100);
			TSubclassOf<AEnemyFlameCharacter> SpawnClassEnemies;
			if (RandVal <= ChanceSpawnMeleeEnemy)
				SpawnClassEnemies = ClassEnemiesMeleeAttack;
			else
				SpawnClassEnemies = ClassEnemiesDistanceAttack;

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = GetInstigator();
			//const FVector SpawnLoc = FVector::ZeroVector;
			

			AEnemyFlameCharacter* NewAIChar = Cast<AEnemyFlameCharacter>(GetWorld()->SpawnActor(SpawnClassEnemies, &SpawnLoc, &SpawnRot, SpawnParams));

			if (NewAIChar)
			{
				NewAIChar->PlController = this;
				
				FString EnemyName;
				switch (NewAIChar->CutEnemyType) {
				case EEnemyType::MeleeEnemy:
					{
						EnemyName = "MeleeEnemy";
						break;
					}
				case EEnemyType::DistanceEnemy:
					{
						EnemyName = "DistanceEnemy";
						break;
					}
				default:
					break;
				}
				UFlameGameInstance* CurGameInstance = Cast<UFlameGameInstance>(GetGameInstance());
			
				if (CurGameInstance->CurEnemiesInfo)
				{
					FEnemiesInfo* Result = CurGameInstance->CurEnemiesInfo->FindRow<FEnemiesInfo>(*EnemyName, *EnemyName, true);
					NewAIChar->SetParams(Result->HealthInPercent, Result->MaxSpeed);

					FString NameNewChar = NewAIChar->GetName();
					
					MapEnemies.Add(NameNewChar, NewAIChar);
				}

				
			}
			
		}
	}
	
	BeginRespawnCountEnemies += IncRespawnCountEnemies;
}


