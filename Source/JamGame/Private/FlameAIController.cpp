// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameAIController.h"

#include "FlameBaseFunctionLibrary.h"
#include "FlameGameInstance.h"
#include "FlameHealthComponent.h"
#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "Kismet/KismetMathLibrary.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Sight.h"


/////////////////////////////
////////	Basic Overloaded
////////////////////////////

void AFlameAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AFlameAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	CurPawn = Cast<AEnemyFlameCharacter>(InPawn);

	switch (CurPawn->CutEnemyType)
	{
	case EEnemyType::MeleeEnemy:
		CurNameOfEnemy = "MeleeEnemy";
		break;
	case EEnemyType::DistanceEnemy:
		CurNameOfEnemy = "DistanceEnemy";
		break;
	default: ;
	}

	TypeWasDefined_BP();
	 
}

void AFlameAIController::SetRadiuses()
{
	const FAISenseID SenseIdFilter_Sight = UAISense::GetSenseID(UAISense_Sight::StaticClass());
	UAISenseConfig_Sight* SightConfiguration_Sight = Cast<UAISenseConfig_Sight>(GetAIPerceptionComponent()->GetSenseConfig(SenseIdFilter_Sight));

	UFlameGameInstance* MyGI = Cast<UFlameGameInstance>(GetGameInstance());
	if (MyGI)
	{
		FEnemiesInfo* Row = MyGI->CurEnemiesInfo->FindRow<FEnemiesInfo>(CurNameOfEnemy, "", false);
	
		if(SightConfiguration_Sight && Row)
		{
			SightConfiguration_Sight->SightRadius = Row->SightRadius;
			SightConfiguration_Sight->LoseSightRadius = Row->LoseSightRadius;
			SightConfiguration_Sight->PeripheralVisionAngleDegrees = Row->PeripheralVisionAngleDegrees;
		}

		const FAISenseID SenseIdFilter_Hearing = UAISense::GetSenseID(UAISense_Hearing::StaticClass());
		UAISenseConfig_Hearing* SightConfiguration_Hearing = Cast<UAISenseConfig_Hearing>(GetAIPerceptionComponent()->GetSenseConfig(SenseIdFilter_Hearing));

		if(SightConfiguration_Hearing && Row)
		{
			SightConfiguration_Hearing->HearingRange = Row->HearingRange;
		}
	}
	
}

void AFlameAIController::TypeWasDefined_BP_Implementation()
{
	// in BP
}

void AFlameAIController::Tick(float DeltaTime)
{
	/*CurDeltaTime += DeltaTime;
	if (CurDeltaTime-PastCurDeltaTime >= TickToDealLogicTime)
	{
		PastCurDeltaTime = CurDeltaTime;
		if (CurPawn && CurPawn->PlController && CurPawn->PlChar)
		{
			if (CurPawn->CutEnemyType == EEnemyType::MeleeEnemy)
			{
				StartMeleeLogic(DeltaTime);
			}
			else if (CurPawn->CutEnemyType == EEnemyType::DistanceEnemy)
			{
				StartDistanceLogic(DeltaTime);
			}
		}
	}*/
	
	Super::Tick(DeltaTime);
}


/////////////////////////////
////////		Enemy Logics
////////////////////////////

void AFlameAIController::StartMeleeLogic(float DeltaTime)
{
	/*RadiusLength = 200;

	if(CurPawn->HealthComponent->bIsAlive)
	{
		FVector DirEnemy = CurPawn->GetActorLocation();
		FVector DirPlayer = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		FVector CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;

		/*switch (CurSide) {
		case ESides::Front:
			CurDirection += CurPawn->PlChar->GetActorForwardVector();
			break;
		case ESides::Back:
			CurDirection += CurPawn->PlChar->GetActorForwardVector() * -30;
			break;
		case ESides::Left:
			CurDirection += CurPawn->PlChar->GetActorRightVector() * -30;
			break;
		case ESides::Right:
			CurDirection += CurPawn->PlChar->GetActorRightVector() * 30;
			break;
		default:
			break;
		}#1#
		
		bIsEqualTargetCommonDir = false;

		bIsSurrounds = false;
		
		MoveDeltaTime += DeltaTime;
		if (FVector::Dist(DirEnemy, DirPlayer) > RadiusLength)
		{
			bIsCanAttack = false;
			CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		}
		else if (bIsCanAttack || FVector::Dist(DirEnemy, DirPlayer) < 100)
		{
			CurPawn->Attack();
			CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		}
		else if (FVector::Dist(PastTargetCircleDir, DirEnemy) < 10)
		{
			bIsCanAttack = true;
			CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		} 
		else
		{
			switch (CurSide) {
			case ESides::Front:
				CurDirection += CurPawn->PlChar->GetActorForwardVector() * RadiusLength;
				break;
			case ESides::Back:
				CurDirection += CurPawn->PlChar->GetActorForwardVector() * -RadiusLength;
				break;
			case ESides::Left:
				CurDirection += CurPawn->PlChar->GetActorRightVector() * -RadiusLength;
				break;
			case ESides::Right:
				CurDirection += CurPawn->PlChar->GetActorRightVector() * RadiusLength;
				break;
			default:
				break;
			}

			PastTargetCircleDir = CurDirection;

			bIsSurrounds = true;
		}

		if (MoveDeltaTime >= 1)
		{
			FHitResult Hit;

			bIsGetRoundLeft = false;
			bIsGetRoundRight = false;
	 
			// We set up a line trace from our current location to a point 1000cm ahead of us
			FVector TraceStartLeft = (DirEnemy + CurPawn->GetActorForwardVector()*80 - CurPawn->GetActorRightVector()*80)*FVector(1, 1, 0.1);
			FVector TraceEndLeft = CurDirection*FVector(1, 1, 0.1);

			FCollisionQueryParams QueryParams;
			QueryParams.AddIgnoredActor(this);
	 
			GetWorld()->LineTraceSingleByChannel(Hit, TraceStartLeft, TraceEndLeft, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartLeft, TraceEndLeft, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);
			
			if (Hit.GetActor())
			{
				if (Hit.GetActor()->GetName() != "BP_FlamePlayerCharacter_C_0" && !Hit.GetActor()->GetName().Contains("Enemy"))
				{
					bIsGetRoundLeft = true;
					if (Hit.Distance <= 160)
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 - CurPawn->GetActorForwardVector()*100;
					}
					else
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 + CurPawn->GetActorForwardVector()*100;
					}
					
				}
			}

			FHitResult HitForward;
			
			FVector TraceStartForward = (DirEnemy + CurPawn->GetActorForwardVector()*80)*FVector(1, 1, 0.1);
			FVector TraceEndForward = CurDirection*FVector(1, 1, 0.1);;

			GetWorld()->LineTraceSingleByChannel(HitForward, TraceStartForward, TraceEndForward, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartForward, TraceEndForward, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);

			if (HitForward.GetActor())
			{
				if (HitForward.GetActor()->GetName() != "BP_FlamePlayerCharacter_C_0" && !HitForward.GetActor()->GetName().Contains("Enemy"))
				{
					bIsGetRoundLeft = true;
					if (Hit.Distance <= 160 || HitForward.Distance <= 160)
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 - CurPawn->GetActorForwardVector()*100;
					}
					else
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 + CurPawn->GetActorForwardVector()*100;
					}
				}
			}
			
			FVector TraceStartRight = (DirEnemy + CurPawn->GetActorForwardVector()*80 + CurPawn->GetActorRightVector()*80)*FVector(1, 1, 0.1);;
			FVector TraceEndRight = CurDirection*FVector(1, 1, 0.1);;
			
			GetWorld()->LineTraceSingleByChannel(Hit, TraceStartRight, TraceEndRight, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartRight, TraceEndRight, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);
			
			if (Hit.GetActor())
			{
				if (Hit.GetActor()->GetName() != "BP_FlamePlayerCharacter_C_0" && !Hit.GetActor()->GetName().Contains("Enemy"))
				{
					bIsGetRoundRight = true;
					if (Hit.Distance <= 160)
					{
						TargetRoundLeft = DirEnemy - CurPawn->GetActorRightVector()*100 - CurPawn->GetActorForwardVector()*100;
					}
					else
					{
						TargetRoundLeft = DirEnemy - CurPawn->GetActorRightVector()*100 + CurPawn->GetActorForwardVector()*100;
					}
				}
			}
			MoveDeltaTime = 0;
			MoveDeltaTimeSec = 0;
		}

		MoveDeltaTimeSec += DeltaTime;

		if (bIsGetRoundLeft && MoveDeltaTimeSec < 0.3)
		{
			CurDirection = TargetRoundLeft;
		}
		else if (bIsGetRoundRight && MoveDeltaTimeSec < 0.3)
		{
			CurDirection = TargetRoundRight;
		}
		
		/*if (!bIsEqualTargetCommonDir)
			CurPawn->RotateTo(CurDirection);
		
		CurPawn->MoveTo(CurDirection);#1#
	}*/
}

void AFlameAIController::StartDistanceLogic(float DeltaTime)
{
	/*RadiusLength = 400;

	if(CurPawn->HealthComponent->bIsAlive)
	{
		FVector DirEnemy = CurPawn->GetActorLocation();
		FVector DirPlayer = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		FVector CurDirection;
		
		bIsEqualTargetCommonDir = false;

		bIsSurrounds = false;
		bIsSafeZone = false;
		
		MoveDeltaTime += DeltaTime;
		if (FVector::Dist(DirEnemy, DirPlayer) > RadiusLength)
		{
			bIsCanAttack = false;
			CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		}
		else if (bIsCanAttack || FVector::Dist(DirEnemy, DirPlayer) < 500)
		{
			CurPawn->Attack();
			bIsSafeZone = true;
			//CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		}
		else if (FVector::Dist(PastTargetCircleDir, DirEnemy) < 10)
		{
			bIsCanAttack = true;
			//CurDirection = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		} 
		else
		{
			switch (CurSide) {
			case ESides::Front:
				CurDirection += CurPawn->PlChar->GetActorForwardVector() * RadiusLength;
				break;
			case ESides::Back:
				CurDirection += CurPawn->PlChar->GetActorForwardVector() * -RadiusLength;
				break;
			case ESides::Left:
				CurDirection += CurPawn->PlChar->GetActorRightVector() * -RadiusLength;
				break;
			case ESides::Right:
				CurDirection += CurPawn->PlChar->GetActorRightVector() * RadiusLength;
				break;
			default:
				break;
			}

			PastTargetCircleDir = CurDirection;

			bIsSurrounds = true;
		}

		if (MoveDeltaTime >= 1)
		{
			FHitResult Hit;

			bIsGetRoundLeft = false;
			bIsGetRoundRight = false;
	 
			// We set up a line trace from our current location to a point 1000cm ahead of us
			FVector TraceStartLeft = (DirEnemy + CurPawn->GetActorForwardVector()*80 - CurPawn->GetActorRightVector()*80)*FVector(1, 1, 0.1);
			FVector TraceEndLeft = CurDirection*FVector(1, 1, 0.1);

			FCollisionQueryParams QueryParams;
			QueryParams.AddIgnoredActor(this);
	 
			GetWorld()->LineTraceSingleByChannel(Hit, TraceStartLeft, TraceEndLeft, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartLeft, TraceEndLeft, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);
			
			if (Hit.GetActor())
			{
				if (Hit.GetActor()->GetName() != "BP_FlamePlayerCharacter_C_0" && !Hit.GetActor()->GetName().Contains("Enemy"))
				{
					bIsGetRoundLeft = true;
					if (Hit.Distance <= 160)
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 - CurPawn->GetActorForwardVector()*100;
					}
					else
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 + CurPawn->GetActorForwardVector()*100;
					}
					
				}
			}

			FHitResult HitForward;
			
			FVector TraceStartForward = (DirEnemy + CurPawn->GetActorForwardVector()*80)*FVector(1, 1, 0.1);
			FVector TraceEndForward = CurDirection*FVector(1, 1, 0.1);;

			GetWorld()->LineTraceSingleByChannel(HitForward, TraceStartForward, TraceEndForward, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartForward, TraceEndForward, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);

			if (HitForward.GetActor())
			{
				if (HitForward.GetActor()->GetName() != "BP_FlamePlayerCharacter_C_0" && !HitForward.GetActor()->GetName().Contains("Enemy"))
				{
					bIsGetRoundLeft = true;
					if (Hit.Distance <= 160 || HitForward.Distance <= 160)
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 - CurPawn->GetActorForwardVector()*100;
					}
					else
					{
						TargetRoundLeft = DirEnemy + CurPawn->GetActorRightVector()*100 + CurPawn->GetActorForwardVector()*100;
					}
				}
			}
			
			FVector TraceStartRight = (DirEnemy + CurPawn->GetActorForwardVector()*80 + CurPawn->GetActorRightVector()*80)*FVector(1, 1, 0.1);;
			FVector TraceEndRight = CurDirection*FVector(1, 1, 0.1);;
			
			GetWorld()->LineTraceSingleByChannel(Hit, TraceStartRight, TraceEndRight, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartRight, TraceEndRight, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);
			
			if (Hit.GetActor())
			{
				if (Hit.GetActor()->GetName() != "BP_FlamePlayerCharacter_C_0" && !Hit.GetActor()->GetName().Contains("Enemy"))
				{
					bIsGetRoundRight = true;
					if (Hit.Distance <= 160)
					{
						TargetRoundLeft = DirEnemy - CurPawn->GetActorRightVector()*100 - CurPawn->GetActorForwardVector()*100;
					}
					else
					{
						TargetRoundLeft = DirEnemy - CurPawn->GetActorRightVector()*100 + CurPawn->GetActorForwardVector()*100;
					}
				}
			}
			MoveDeltaTime = 0;
			MoveDeltaTimeSec = 0;
		}

		MoveDeltaTimeSec += DeltaTime;

		if (bIsGetRoundLeft && MoveDeltaTimeSec < 0.3)
		{
			CurDirection = TargetRoundLeft;
		}
		else if (bIsGetRoundRight && MoveDeltaTimeSec < 0.3)
		{
			CurDirection = TargetRoundRight;
		}*/
		
		/*if (!bIsEqualTargetCommonDir && !bIsSafeZone)
			CurPawn->RotateTo(CurDirection);
		if (!bIsCanAttack && !bIsSafeZone)
			CurPawn->MoveTo(CurDirection);*/
	//}

		
		/*FVector DirEnemy = CurPawn->GetActorLocation();
		FVector DirPlayer = CurPawn->PlChar->GetActorLocation() + ConstDeltaZ;
		
		float DistBetweenAct = FVector::Dist(DirPlayer, DirEnemy);

		DistBetweenAct < SafeDistance ? bIsSafeZone = false : bIsSafeZone = true;
		DistBetweenAct < AttackDistance ? bIsCanAttack = true : bIsCanAttack = false;

		MoveDeltaTime += DeltaTime;
		if (!bIsSafeZone)
		{
			FVector NewVector = UKismetMathLibrary::GetDirectionUnitVector(CurPawn->PlChar->GetActorLocation(), CurPawn->GetActorLocation())*FVector(1, 1, 0);
			NewVector.Normalize();
			
			if (MoveDeltaTime >= 1)
			{
				if (PastVectorPosition == DirEnemy)
				{
					CurPawn->Jump();
				}
				MoveDeltaTime = 0;
				PastVectorPosition = DirEnemy;
			}
			
			CurPawn->RotateTo(DirEnemy + NewVector);
			CurPawn->MoveTo(DirEnemy + NewVector);

			
		}
		else if (!bIsCanAttack)
		{
			MoveDeltaTime += DeltaTime;
			if (MoveDeltaTime >= 0.1) {
				CurPawn->RotateTo(DirPlayer);
				MoveDeltaTime = 0;
			}
			CurPawn->MoveTo(DirPlayer);
		}
		else if (bIsSafeZone == true && bIsCanAttack == true)
		{
			MoveDeltaTime += DeltaTime;
			if (MoveDeltaTime >= 0.1) {
				CurPawn->RotateTo(DirPlayer);
				MoveDeltaTime = 0;
			}
			CurPawn->Attack();
		}*/
		
	
}