// Fill out your copyright notice in the Description page of Project Settings.

//#include "FlameHealthComponent.h"
#include "EnemyFlameCharacter.h"

#include "FlameBonusActor.h"
#include "FlameGameInstance.h"
#include "FlameHealthComponent.h"
#include "FlamePlayerController.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

AEnemyFlameCharacter::AEnemyFlameCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*if (!CollisionComponent)
	{
		// Use a sphere as a simple collision representation.
		CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
		// Set the sphere's collision profile name to "Projectile".
		CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Enemy"));
		// Set the sphere's collision radius.
		CollisionComponent->InitSphereRadius(15.0f);
		// Set the root component to be the collision component.
		RootComponent = CollisionComponent;
	}

	if (!PawnSkeletalMesh)
	{
		PawnSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh "));
		PawnSkeletalMesh->SetGenerateOverlapEvents(false);
		PawnSkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
		PawnSkeletalMesh->SetupAttachment(RootComponent);
	}*/

	// HealthComponent
	HealthComponent = CreateDefaultSubobject<UFlameHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AEnemyFlameCharacter::PawnDead);
		HealthComponent->OnHealthChange.AddDynamic(this, &AEnemyFlameCharacter::PawnHealthChange);
	}
}

void AEnemyFlameCharacter::PawnDead()
{

	if (PlController)
		PlController->NotifyEnemyDeath();

	HealthComponent->bIsAlive = false;
	
	// ignore Projectile
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel3, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel3, ECollisionResponse::ECR_Ignore);
	// ignore Bonus
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel4, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel4, ECollisionResponse::ECR_Ignore);
	
	float TimeToSpawnBonus = 1.0f;
	if (AnimDeath)
	{
		TimeToSpawnBonus = AnimDeath->GetPlayLength() - 0.4;
		PlayDeathAnim_BP();
	}
	GetWorldTimerManager().SetTimer(TimerHandle_PlayingDeath,
										this,
										&AEnemyFlameCharacter::SpawnBonus,
										TimeToSpawnBonus,
										true);
	
}

void AEnemyFlameCharacter::PawnHealthChange(float Health, float Damage)
{
}

void AEnemyFlameCharacter::BeginPlay()
{
	Super::BeginPlay();

	
}

float AEnemyFlameCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	if (HealthComponent->bIsAlive)
	{
		PlayDamageAnim_BP();
		HealthComponent->ChangeHealthValue(-Damage);
	}
		

	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AEnemyFlameCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentDeltaTime += DeltaTime;
}

void AEnemyFlameCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	PlController = Cast<AFlamePlayerController>(GetWorld()->GetFirstPlayerController());
	PlChar = Cast<APlayerFlameCharacter>(PlController->GetPawn());

	if (CutEnemyType == EEnemyType::MeleeEnemy)
	{
		CurrentWeapon = UFlameBaseFunctionLibrary::SetWeapon(GetWorld(), GetMesh(), this,
													 GetInstigator(), PlController, "EnemyMeleeWeapon");
	}
	else if (CutEnemyType == EEnemyType::DistanceEnemy)
	{
		CurrentWeapon = UFlameBaseFunctionLibrary::SetWeapon(GetWorld(), GetMesh(), this,
													 GetInstigator(), PlController, "EnemyDistanceWeapon");
	}
	
	
}

void AEnemyFlameCharacter::Attack()
{
	if (PlChar && CurrentWeapon && CurrentDeltaTime - PastShootingTime >= CurrentWeapon->CurrentWeaponInfo.RateOfFire)
	{
		PlayAttackAnim_BP();
		CurrentWeapon->Fire();
		PastShootingTime = CurrentDeltaTime;
	}
}


void AEnemyFlameCharacter::SetParams(int HealthInPercent, int MaxSpeed)
{
	HealthComponent->MaxHealth = HealthInPercent;
	HealthComponent->Health = HealthInPercent;
	GetCharacterMovement()->MaxWalkSpeed = MaxSpeed;
}


/////////////////////////////
////////	Movement
////////////////////////////

void AEnemyFlameCharacter::SpawnBonus()
{
	
	GetWorldTimerManager().ClearTimer(TimerHandle_PlayingDeath);
	
	if (UFlameGameInstance* MyGI = Cast<UFlameGameInstance>(GetGameInstance()))
	{
		FBonus NewInfoBonus;
		MyGI->GetRandomBonus(NewInfoBonus);

		const FVector CurSpawnLocation = GetActorLocation();
		const FRotator CurSpawnRotation = FRotator::ZeroRotator;

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			
		AFlameBonusActor* NewBonus = Cast<AFlameBonusActor>(GetWorld()->SpawnActor(NewInfoBonus.BonusClass,
																	&CurSpawnLocation,
																	&CurSpawnRotation,
																	SpawnParams));
		if (NewBonus)
		{
			NewBonus->CurrentBonusInfo = NewInfoBonus;
		}
		
	}

	Destroy();
}

void AEnemyFlameCharacter::ChangeSpeed(float FactorSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed *= FactorSpeed;
}

void AEnemyFlameCharacter::PlayAttackAnim_BP_Implementation()
{
	// in BP
}

void AEnemyFlameCharacter::PlayDamageAnim_BP_Implementation()
{
	// in BP
}

void AEnemyFlameCharacter::PlayDeathAnim_BP_Implementation()
{
	// in BP
}

////////////////////////////////
/////// Bonus overriding methods 
////////////////////////////////

/*void AEnemyFlameCharacter::Bonus_AddCoins(float IncVal)
{
	IFlameBonusObjInterface::Bonus_AddCoins(IncVal);
}

void AEnemyFlameCharacter::Bonus_AddHealth(float IncVal)
{
	IFlameBonusObjInterface::Bonus_AddHealth(IncVal);
}

void AEnemyFlameCharacter::Bonus_AddRespawn(float IncVal)
{
	IFlameBonusObjInterface::Bonus_AddRespawn(IncVal);
}

void AEnemyFlameCharacter::Bonus_IncSpeedWalk(float IncVal)
{
	IFlameBonusObjInterface::Bonus_IncSpeedWalk(IncVal);
}

void AEnemyFlameCharacter::Bonus_IncDamage(float IncVal)
{
	IFlameBonusObjInterface::Bonus_IncDamage(IncVal);
}

void AEnemyFlameCharacter::Bonus_IncProjectileSpeed(float IncVal)
{
	IFlameBonusObjInterface::Bonus_IncProjectileSpeed(IncVal);
}*/
