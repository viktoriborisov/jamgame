// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameHealthComponent.h"

// Sets default values for this component's properties
UFlameHealthComponent::UFlameHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFlameHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UFlameHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


/** get current health */
float UFlameHealthComponent::GetCurrentHealth()
{
	return Health;
}


/** set current health */
void UFlameHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}


/** change value of current health */
void UFlameHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue *= CoefDamage;
	
	Health += ChangeValue;
	OnHealthChange.Broadcast(Health, ChangeValue);
	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health <= 0.0f)
		{
			CountLife -= 1;
			if (CountLife > 0)
			{
				Health = 100.0f;
				OnChangeCountLive.Broadcast(CountLife);
			}
			else
			{
				bIsAlive = false;
				OnDead.Broadcast();
			}
		}
	}
}
