// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameBonusObjInterface.h"



// Add default functionality here for any IFlameBonusObjInterface functions that are not pure virtual.
void IFlameBonusObjInterface::Bonus_AddCoins(float IncVal)
{
}

void IFlameBonusObjInterface::Bonus_AddHealth(float IncVal)
{
}

void IFlameBonusObjInterface::Bonus_AddRespawn(float IncVal)
{
}

void IFlameBonusObjInterface::Bonus_IncSpeedWalk(float IncVal, float Time)
{
}

void IFlameBonusObjInterface::Bonus_IncDamage(float IncVal, float Time)
{
}

void IFlameBonusObjInterface::Bonus_IncProjectileSpeed(float IncVal, float Time)
{
}


void IFlameBonusObjInterface::Bonus_ApplyThumb(float IncVal)
{
	
}


void IFlameBonusObjInterface::Bonus_ApplyMiddle(float IncVal)
{
	
}

void IFlameBonusObjInterface::Bonus_ApplyRing(float IncVal)
{
	
}

void IFlameBonusObjInterface::Bonus_ApplyLittle(float IncVal)
{
	
}