// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameProjectileActor.h"

#include "FlameBaseFunctionLibrary.h"
#include "PlayerFlameCharacter.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AFlameProjectileActor::AFlameProjectileActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!RootComponent)
    {
        RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSceneComponent"));
    }

    if (!CollisionComponent)
    {
        // Use a sphere as a simple collision representation.
        CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
        // Set the sphere's collision profile name to "Projectile".
        CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Projectile"));
        // Event called when component hits something.
        CollisionComponent->OnComponentHit.AddDynamic(this, &AFlameProjectileActor::OnHit);
        // Set the sphere's collision radius.
        CollisionComponent->InitSphereRadius(15.0f);
        // Set the root component to be the collision component.
        RootComponent = CollisionComponent;
    }

    if (!ProjectileMovementComponent)
    {
        // Use this component to drive this projectile's movement.
        ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
        ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
        ProjectileMovementComponent->InitialSpeed = 1.f;
        ProjectileMovementComponent->MaxSpeed = 0.f;
        ProjectileMovementComponent->bRotationFollowsVelocity = true;
        ProjectileMovementComponent->bShouldBounce = true;
        ProjectileMovementComponent->Bounciness = 0.3f;
        ProjectileMovementComponent->ProjectileGravityScale = 0.0f;
    }

    if (!TrailFX)
    {
        TrailFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TrailFX FX"));
        TrailFX->SetupAttachment(RootComponent);
    }

    if (!ProjectileSkeletalMesh)
    {
        ProjectileSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh "));
        ProjectileSkeletalMesh->SetGenerateOverlapEvents(false);
        ProjectileSkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
        ProjectileSkeletalMesh->SetupAttachment(RootComponent);
    }

    if (!ProjectileMesh)
    {
        ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
        ProjectileMesh->SetupAttachment(RootComponent);
        ProjectileMesh->SetCanEverAffectNavigation(false);
    }
	
}

// Called when the game starts or when spawned
void AFlameProjectileActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFlameProjectileActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFlameProjectileActor::InitProjectile(AActor* CurWeaponOwner,
										   const FProjectileSetting NewProjectileSetting,
										   const FVector& ShootDirection,
										   float InputDamage,
										   float InputMaxDamageDistance,
										   float InputMinDamageDistance)
{
	WeaponOwner = CurWeaponOwner;
	ProjectileSetting = NewProjectileSetting;
	Damage = InputDamage;
	MaxDamageDistance = InputMaxDamageDistance;
	MinDamageDistance = InputMinDamageDistance;
    
	InitialLifeSpan = ProjectileSetting.ProjectileLifeTime;
	ProjectileMesh = ProjectileSetting.ProjectileStaticMesh;

	TrailFX->SetTemplate(ProjectileSetting.ProjectileTrailFX);
	
	MoveToDirection(ShootDirection);
    
}

/** set end direction for projectile */
void AFlameProjectileActor::MoveToDirection(const FVector& ShootDirection) const
{
	FVector Direction = ShootDirection - GetActorLocation();
	Direction.Normalize();
    
	ProjectileMovementComponent->InitialSpeed = ProjectileSetting.InitialSpeed;
	ProjectileMovementComponent->MaxSpeed = ProjectileSetting.MaxSpeed;
    
	ProjectileMovementComponent->Velocity = Direction * ProjectileMovementComponent->InitialSpeed;
}

void AFlameProjectileActor::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
    if (OtherActor != nullptr && OtherActor != this && OtherComponent != nullptr && this->GetClass() != OtherActor->GetClass())
    {
        //TODO это костыль, надо будет переделать
        if (!(Cast<APlayerFlameCharacter>(OtherActor) && ProjectileSetting.OwnerType == EOwnerType::Player))
        {
        	if (ProjectileSetting.ProjectileType == EProjectileType::Simple)
        	{
        		UGameplayStatics::ApplyPointDamage(OtherActor,
											   Damage,
											   Hit.TraceStart,
											   Hit,
											   GetInstigatorController(),
											   WeaponOwner,
											   nullptr);
        	}
        	else if (ProjectileSetting.ProjectileType == EProjectileType::Grenade)
        	{
        		if (ProjectileSetting.ExploseFX)
        		{
        			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), FRotator::ZeroRotator, FVector(0.2f));
        		}
        		if (ProjectileSetting.ExploseSound)
        		{
        			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
        		}
        		
        		TArray<AActor*> IgnoredActor = { WeaponOwner };
        		UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
					Damage,
					Damage*0.2f,
					GetActorLocation(),
					ProjectileSetting.MaxDamageDistance,
					ProjectileSetting.MinDamageDistance,
					5,
					nullptr, IgnoredActor, this, GetInstigatorController());

        	}
        }
    }

    Destroy();
}