// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameGameInstance.h"

#include "FlameBaseFunctionLibrary.h"
#include "JsonObjectConverter.h"

bool UFlameGameInstance::GetWeaponInfoByName(const FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (CurWeaponInfo)
	{
		WeaponInfoRow = CurWeaponInfo->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UArenaGameInstance::GetWeaponInfoByName - Weapon table -NULL"));
	}
	
	return bIsFind;
}

bool UFlameGameInstance::GetBonusInfoByName(FName NameBonus, FBonus& OutInfo)
{
	bool bIsFind = false;
	FBonus* BonusInfoRow;

	if (CurBonusInfo)
	{
		BonusInfoRow = CurBonusInfo->FindRow<FBonus>(NameBonus, "", false);
		if (BonusInfoRow)
		{
			bIsFind = true;
			OutInfo = *BonusInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UArenaGameInstance::GetWeaponInfoByName - Weapon table -NULL"));
	}
	
	return bIsFind;
}


bool UFlameGameInstance::GetEnemiesInfoByName(FName NameEnemy, FEnemiesInfo& OutInfo)
{
	bool bIsFind = false;
	FEnemiesInfo* EnemiesInfoRow;

	if (CurEnemiesInfo)
	{
		EnemiesInfoRow = CurEnemiesInfo->FindRow<FEnemiesInfo>(NameEnemy, "", false);
		if (EnemiesInfoRow)
		{
			bIsFind = true;
			OutInfo = *EnemiesInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UArenaGameInstance::GetEnemiesInfoByName - Enemies table -NULL"));
	}
	
	return bIsFind;
}


void UFlameGameInstance::GetRandomBonus(FBonus& OutInfo)
{
	if (CurBonusInfo)
	{
		TArray<FName> RowNames;
		RowNames = CurBonusInfo->GetRowNames();
		/*float MaxChance = 0.0f;

		for ( auto& name : RowNames )
		{
			FBonus* BonusInfoRow = CurBonusInfo->FindRow<FBonus>(name, "", false);

			MaxChance += BonusInfoRow->ChanceToSpawn;
		}*/

		float CurRandSign = FMath::RandRange(1, 100);
		float SumChance = 0.0f;

		for ( auto& name : RowNames )
		{
			FBonus* BonusInfoRow = CurBonusInfo->FindRow<FBonus>(name, "", false);

			SumChance += BonusInfoRow->ChanceToSpawn;

			if (CurRandSign <= SumChance)
			{
				OutInfo = *BonusInfoRow;
				break;
			}
		}
	}
}

FString UFlameGameInstance::GetPathToDB()
{
	FString Path = FPaths::ProjectDir();
	/*FString Path = UKismetSystemLibrary::GetPlatformUserDir();
	Path.Append("IToaster/");*/
	Path.Append("Tables/");
	Path.Append("db.json");
	return Path;
}

void UFlameGameInstance::SaveDB()
{

	FString SerData;

	CurDBJson.bIsFirstLaunch = bIsFirstLaunch;
	CurDBJson.CurPlayerName = CurPlayerName;
	CurDBJson.DiffLevel = static_cast<int>(DiffLevel);
	CurDBJson.Coins = Coins;
	CurDBJson.MainSoundVolume = MainSoundVolume;
	CurDBJson.EffectSoundVolume = EffectSoundVolume;
	CurDBJson.Skills = Skills;
	CurDBJson.Records = Records;

		
	FJsonObjectConverter::UStructToJsonObjectString(CurDBJson, SerData);
	FFileHelper::SaveStringToFile(SerData, ToCStr(GetPathToDB()));
}

bool UFlameGameInstance::LoadDB()
{
	bool Res = true;
	FString SerData;
	FFileHelper::LoadFileToString(SerData, ToCStr(GetPathToDB()));

	if(!FJsonObjectConverter::JsonObjectStringToUStruct(SerData, &CurDBJson, 0, 0))
	{
		Res = false;
	}
	else
	{
		bIsFirstLaunch = CurDBJson.bIsFirstLaunch;
		CurPlayerName = CurDBJson.CurPlayerName;
		DiffLevel = static_cast<EDifficultyLevel>(CurDBJson.DiffLevel);
		Coins = CurDBJson.Coins;
		MainSoundVolume = CurDBJson.MainSoundVolume;
		EffectSoundVolume = CurDBJson.EffectSoundVolume;
		Skills = CurDBJson.Skills;
		Records = CurDBJson.Records;
	}
	return Res;
}


TArray<FString> UFlameGameInstance::GetCSVFile(FString Path)
{
	TArray<FString> Lines;
	FString CsvFile = /*FPaths::ProjectContentDir() +*/ Path;
	if (FPaths::FileExists(CsvFile))
	{
		UE_LOG(LogTemp, Log, TEXT("File %s exists"), *CsvFile);		
		FFileHelper::LoadANSITextFileToStrings(*CsvFile, NULL, Lines);
	}

	return Lines;
}


void  UFlameGameInstance::LoadCSVDataFile(ECSVTableType CurTableType, EDifficultyLevel CurDiffLevel)
{
	FString Path = FPaths::ProjectDir();
	/*FString Path = UKismetSystemLibrary::GetPlatformUserDir();
	Path.Append("IToaster");*/
	Path.Append("/Tables");
	//UClass* DataTableClass = UDataTable::StaticClass(); // get a reference to the type of object we are going to use, in this case the basic DataTable, but you can have your own
	//UDataTable* SelectedClassProgressionDataTable = nullptr;

	DiffLevel = CurDiffLevel;
	
	switch (CurDiffLevel) {
	case EDifficultyLevel::Simple:
		Path.Append("/Simple");
		break;
	case EDifficultyLevel::Middle:
		Path.Append("/Middle");
		break;
	case EDifficultyLevel::Hard:
		Path.Append("/Hard");
		break;
	default: ;
	}
	
	TArray<FString> CSVLines;
	
	if (CurTableType == ECSVTableType::Levels)
	{
		/*SelectedClassProgressionDataTable = NewObject<UDataTable>(this, DataTableClass, FName(TEXT("Levels")));
		SelectedClassProgressionDataTable->RowStruct = FLevelInfo::StaticStruct();*/
		Path.Append("/Levels.csv");
		CSVLines = GetCSVFile(Path);
	}
	else if (CurTableType == ECSVTableType::Enemies)
	{
		/*SelectedClassProgressionDataTable = NewObject<UDataTable>(this, DataTableClass, FName(TEXT("Enemies")));
		SelectedClassProgressionDataTable->RowStruct = FEnemiesInfo::StaticStruct();*/
		Path.Append("/Enemies.csv");
		CSVLines = GetCSVFile(Path);

	}
	else if (CurTableType == ECSVTableType::Bonuses)
	{
		/*SelectedClassProgressionDataTable = NewObject<UDataTable>(this, DataTableClass, FName(TEXT("Bonuses")));
		SelectedClassProgressionDataTable->RowStruct = FBonus::StaticStruct();*/
		Path.Append("/Bonuses.csv");
		CSVLines = GetCSVFile(Path);

	}
	else if (CurTableType == ECSVTableType::Weapons)
	{
		/*SelectedClassProgressionDataTable = NewObject<UDataTable>(this, DataTableClass, FName(TEXT("Weapons")));
		SelectedClassProgressionDataTable->RowStruct = FWeaponInfo::StaticStruct();*/
		Path.Append("/Weapons.csv");
		CSVLines = GetCSVFile(Path);

	}
	
	
	
	
	for (int i = 1; i < CSVLines.Num(); i++)
	{
		FString aString = CSVLines[i];
		TArray<FString> stringArray = {};
		aString.ParseIntoArray(stringArray, TEXT(";"), false);
		if (stringArray.Num() == 0){continue;}
		FString LineLabel = stringArray[0];
		if ((LineLabel.Len() == 0)  || LineLabel.StartsWith("\";") || LineLabel.StartsWith(";"))
		{
			continue;
		}

		switch (CurTableType) {
			case ECSVTableType::Levels:
			{
				FLevelInfo* Row = CurLevelInfo->FindRow<FLevelInfo>(FName(*stringArray[0]), "", false);
				Row->BeginRespawnCountEnemies = FCString::Atoi(*stringArray[1]);
				Row->MaxRespawnCountEnemies = FCString::Atoi(*stringArray[2]);
				Row->IncRespawnCountEnemies = FCString::Atoi(*stringArray[3]);
				Row->TimeToRespawnEnemies = FCString::Atof(*stringArray[4]);
				Row->ChanceSpawnMeleeEnemy = FCString::Atof(*stringArray[5]);
				Row->ChanceSpawnDistanceEnemy = FCString::Atof(*stringArray[6]);
				break;
			}
			case ECSVTableType::Enemies:
			{
				FEnemiesInfo* Row = CurEnemiesInfo->FindRow<FEnemiesInfo>(FName(*stringArray[0]), "", false);
				Row->HealthInPercent = FCString::Atoi(*stringArray[1]);
				Row->MaxSpeed = FCString::Atoi(*stringArray[2]);
				Row->SightRadius = FCString::Atoi(*stringArray[3]);
				Row->LoseSightRadius = FCString::Atoi(*stringArray[4]);
				Row->PeripheralVisionAngleDegrees = FCString::Atoi(*stringArray[5]);
				Row->HearingRange = FCString::Atoi(*stringArray[6]);
				Row->TeamRange = FCString::Atoi(*stringArray[7]);
				break;
			}
			case ECSVTableType::Bonuses:
			{
				FBonus* Row = CurBonusInfo->FindRow<FBonus>(FName(*stringArray[0]), "", false);
				Row->IncValue = FCString::Atoi(*stringArray[1]);
				Row->ChanceToSpawn = FCString::Atoi(*stringArray[2]);
				Row->LifeTime = FCString::Atoi(*stringArray[3]);
				Row->Cost = FCString::Atoi(*stringArray[4]);
				break;
			}
			case ECSVTableType::Weapons:
			{
				FWeaponInfo* Row = CurWeaponInfo->FindRow<FWeaponInfo>(FName(*stringArray[0]), "", false);
				Row->RateOfFire = FCString::Atoi(*stringArray[1]);
				Row->Damage = FCString::Atoi(*stringArray[2]);
				break;
			}
			default: ;
		}
	}
	
	//return SelectedClassProgressionDataTable;
}