// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerFlameCharacter.h"

#include "FlameGameInstance.h"
#include "FlameHealthComponent.h"
#include "FlamePlayerController.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

APlayerFlameCharacter::APlayerFlameCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = true;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// HealthComponent
	HealthComponent = CreateDefaultSubobject<UFlameHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &APlayerFlameCharacter::CharDead);
		HealthComponent->OnHealthChange.AddDynamic(this, &APlayerFlameCharacter::CharHealthChange);
	}
	
}

void APlayerFlameCharacter::LockClickFingers()
{
	IsActiveClickFingers = false;
	OnChangeClickFingers.Broadcast(false);
	GetWorldTimerManager().SetTimer(TimerHandle_ActivateClickFingers,
									this,
									&APlayerFlameCharacter::ActivateClickFingers,
									TimeToActiveClickFingers,
									false);
}

void APlayerFlameCharacter::ActivateClickFingers()
{
	IsActiveClickFingers = true;
	OnChangeClickFingers.Broadcast(true);
	GetWorldTimerManager().ClearTimer(TimerHandle_ActivateClickFingers);
}

void APlayerFlameCharacter::BeginPlay()
{
	
	UseWeapon("PlayerPistol", EAttackType::Distance, false);
	UseWeapon("SlitWeapon", EAttackType::Melee, false);
	
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	if (UFlameGameInstance* MyGI = Cast<UFlameGameInstance>(GetGameInstance()))
	{
		if (MyGI->Skills.Num() >= 4)
			bIsOpenClickFingers = true;
	}
}

void APlayerFlameCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	UseWeapon("PlayerPistol", EAttackType::Distance, false);
	UseWeapon("SlitWeapon", EAttackType::Melee, false);
}

float APlayerFlameCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	if (HealthComponent->bIsAlive)
		HealthComponent->ChangeHealthValue(-Damage);
	
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void APlayerFlameCharacter::PlayTakeDamageSound_BP_Implementation()
{
	// in BP
}

void APlayerFlameCharacter::PlayDeadSound_BP_Implementation()
{
	// in BP
}

void APlayerFlameCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsAiming)
	{
		FVector WorldLocation, WorldDirection;
		CurrentWeapon->CurPlayerController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);

		/*FRotator dd = (WorldLocation + WorldDirection*50000 - GetActorLocation()).Rotation();

		dd.Pitch = 0;
		
		SetActorRotation(dd);*/

		// find out which way is forward
		/*const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction_X = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		const FVector Direction_Y = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		const FVector Direction_Z = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Z);
		AddMovementInput(Direction_X+Direction_Y+Direction_Z, 0);*/
	}
}

void APlayerFlameCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("FireAction", IE_Released, this, &APlayerFlameCharacter::FireAction);
	
	PlayerInputComponent->BindAction("AimingAction", IE_Pressed, this, &APlayerFlameCharacter::StartAimingAction);
	PlayerInputComponent->BindAction("AimingAction", IE_Released, this, &APlayerFlameCharacter::EndAimingAction);
	PlayerInputComponent->BindAction("ClickFingers", IE_Pressed, this, &APlayerFlameCharacter::ClickFingers);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerFlameCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerFlameCharacter::MoveRight);

	

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);
	}

}



/** MoveForward for character  */
void APlayerFlameCharacter::MoveForward(float Value)
{
	if (Controller != nullptr && Value != 0.0f && !bIsAiming && !bIsAttack)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}


/** MoveRight for character  */
void APlayerFlameCharacter::MoveRight(float Value)
{
	if (Controller != nullptr && Value != 0.0f && !bIsAiming && !bIsAttack)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void APlayerFlameCharacter::ClickFingers()
{
	if (bIsOpenClickFingers && IsActiveClickFingers)
		PlayShootAnimation_BP(EWeaponType::ClickFingers);
}

void APlayerFlameCharacter::UseWeapon(FName NameWeapon, EAttackType CurAttackType, bool IsOnce)
{
	if (AFlamePlayerController* CurrentController = Cast<AFlamePlayerController>(GetController()))
	{
		switch (CurAttackType) {
			case EAttackType::Melee:
				{
					if (CurrentMeleeWeapon && !IsOnce)
						CurrentMeleeWeapon->Destroy();

					AFlameWeaponActor* NewWeapon = UFlameBaseFunctionLibrary::SetWeapon(GetWorld(), GetMesh(),
																						this, GetInstigator(),
																						CurrentController, NameWeapon);
					
					!IsOnce ? CurrentMeleeWeapon = NewWeapon : NewWeapon->Fire();
				}
				break;
			case EAttackType::Distance:
				{
					if (CurrentWeapon && !IsOnce)
						CurrentWeapon->Destroy();

					AFlameWeaponActor* NewWeapon = UFlameBaseFunctionLibrary::SetWeapon(GetWorld(), GetMesh(),
																						this, GetInstigator(),
																						CurrentController, NameWeapon);
					
					!IsOnce ? CurrentWeapon = NewWeapon : NewWeapon->Fire();
				}
				break;
			default: ;
		}
		
	}
}

/** start after action "FireAction" only on client Controller */
void APlayerFlameCharacter::FireAction()
{
	if (bIsAiming)
	{
		PlayShootAnimation_BP(CurrentWeapon->CurrentWeaponInfo.WeaponType);
		CurrentWeapon->Fire();
	}
	else
	{
		PlayShootAnimation_BP(CurrentMeleeWeapon->CurrentWeaponInfo.WeaponType);
	}
}

/** start after IE_Pressed action "AimingAction"  */
void APlayerFlameCharacter::StartAimingAction()
{
	PlayAimAnimation_BP();

	StartAimingAction_BP();
	/*APlayerController* MyController = Cast<APlayerController>(GetController());
	if (MyController)
	{
		CenterViewportCursor(MyController);
		//MyController->bShowMouseCursor = true;
		MyController->bEnableClickEvents = true;
		MyController->bEnableMouseOverEvents = true;
	}*/

	bIsAiming = true;
}

void APlayerFlameCharacter::EndAimingAction_BP_Implementation()
{
}

void APlayerFlameCharacter::StartAimingAction_BP_Implementation()
{
}

/** start after IE_Released action "AimingAction"  */
void APlayerFlameCharacter::EndAimingAction()
{
	/*AFlamePlayerController* MyController = Cast<AFlamePlayerController>(GetController());
	if (MyController)
	{
		MyController->bShowMouseCursor = false;
		MyController->bEnableClickEvents = true;
		MyController->bEnableMouseOverEvents = true;#1#
		MyController->SetMouse();
	}*/
	EndAimingAction_BP();
	
	bIsAiming = false;
}

void APlayerFlameCharacter::CenterViewportCursor(const APlayerController* PlayerController)
{
	if( PlayerController )
	{
		const ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer();
		if( LocalPlayer && LocalPlayer->ViewportClient )
		{
			FViewport* Viewport = LocalPlayer->ViewportClient->Viewport;
			if( Viewport )
			{
				FVector2D ViewportSize;
				LocalPlayer->ViewportClient->GetViewportSize(ViewportSize);
				const int32 X = static_cast<int32>(ViewportSize.X * 0.5f);
				const int32 Y = static_cast<int32>(ViewportSize.Y * 0.5f);

				Viewport->SetMouse(X, Y);
			}
		}
	}
}



void APlayerFlameCharacter::PlayAimAnimation_BP_Implementation()
{
	// in BP
}


void APlayerFlameCharacter::PlayShootAnimation_BP_Implementation(EWeaponType CurWeaponType)
{
}

void APlayerFlameCharacter::CharDead()
{
	PlayDeadSound_BP();
}

void APlayerFlameCharacter::CharHealthChange(float Health, float Damage)
{
	PlayTakeDamageSound_BP();
}

////////////////////////////////
/////// Bonus overriding methods 
////////////////////////////////

void APlayerFlameCharacter::Bonus_AddCoins(float IncVal)
{
	IFlameBonusObjInterface::Bonus_AddCoins(IncVal);
	
	if (UFlameGameInstance* MyGI = Cast<UFlameGameInstance>(GetGameInstance()))
	{
		MyGI->Coins += IncVal;
		OnCoinsChange.Broadcast(MyGI->Coins);
	}
}

void APlayerFlameCharacter::Bonus_AddHealth(float IncVal)
{
	IFlameBonusObjInterface::Bonus_AddHealth(IncVal);

	if (HealthComponent)
	{
		HealthComponent->ChangeHealthValue(IncVal);
	}
}

void APlayerFlameCharacter::Bonus_AddRespawn(float IncVal)
{
	IFlameBonusObjInterface::Bonus_AddRespawn(IncVal);

	if (HealthComponent)
	{
		HealthComponent->CountLife += 1;
		HealthComponent->OnChangeCountLive.Broadcast(HealthComponent->CountLife);
	}
}

void APlayerFlameCharacter::Bonus_IncSpeedWalk(float IncVal, float Time)
{
	IFlameBonusObjInterface::Bonus_IncSpeedWalk(IncVal, Time);

	if (GetMovementComponent())
	{
		GetCharacterMovement()->MaxWalkSpeed *=IncVal;
		if (Time != 0)
			OnActivateSpeedBonus.Broadcast(Time);
	}
	
}

void APlayerFlameCharacter::Bonus_IncDamage(float IncVal, float Time)
{
	IFlameBonusObjInterface::Bonus_IncDamage(IncVal, Time);

	if (CurrentWeapon)
	{
		CurrentWeapon->CurrentWeaponInfo.Damage *= IncVal;
		if (Time != 0)
			OnActivateDamageBonus.Broadcast(Time);
	}
}

void APlayerFlameCharacter::Bonus_IncProjectileSpeed(float IncVal, float Time)
{
	IFlameBonusObjInterface::Bonus_IncProjectileSpeed(IncVal, Time);

	if (CurrentWeapon)
	{
		CurrentWeapon->CurrentWeaponInfo.ProjectileSetting.InitialSpeed *= IncVal;
		CurrentWeapon->CurrentWeaponInfo.ProjectileSetting.MaxSpeed *= IncVal;
		if (Time != 0)
			OnActivateProjSpeedBonus.Broadcast(Time);
	}
	
}

void APlayerFlameCharacter::Bonus_ApplyThumb(float IncVal)
{
	IFlameBonusObjInterface::Bonus_ApplyThumb(IncVal);

	if (CurrentWeapon)
	{
		CurrentWeapon->CurrentWeaponInfo.Damage *= IncVal;
	}
}

void APlayerFlameCharacter::Bonus_ApplyMiddle(float IncVal)
{
	IFlameBonusObjInterface::Bonus_ApplyMiddle(IncVal);

	UseWeapon("PlayerGrenade", EAttackType::Distance, false);
}

void APlayerFlameCharacter::Bonus_ApplyRing(float IncVal)
{
	IFlameBonusObjInterface::Bonus_ApplyRing(IncVal);

	if (GetMovementComponent())
	{
		GetCharacterMovement()->MaxWalkSpeed *=IncVal;
	}
}

void APlayerFlameCharacter::Bonus_ApplyLittle(float IncVal)
{
	IFlameBonusObjInterface::Bonus_ApplyLittle(IncVal);

	if (HealthComponent)
	{
		HealthComponent->CountLife += IncVal;
		HealthComponent->OnChangeCountLive.Broadcast(HealthComponent->CountLife);
	}
}
