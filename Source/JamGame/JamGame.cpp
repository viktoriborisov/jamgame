// Copyright Epic Games, Inc. All Rights Reserved.

#include "JamGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JamGame, "JamGame" );
